const Actualite = require("../models/actualite.model");

var controller = {
    addOne : async (req, res, next) => {
        try {
          const actualite = {"titre": req.body.titre, "description": req.body.description,"datePublication": Date.now()};
          console.log(actualite);
          let actu = await Actualite.create(actualite);
          res.json(actu);
        }catch (err){
          console.error(err);
          next(err);
        }
    },

    getAll: async (req, res, next) => {
        try {
          let actu = await Actualite.find({});
          res.json(actu);
        }catch (err){
          console.error(err);
          next(err);
        }
    },

    updateOne: async (req, res, next) => {
        try {
            let actualite =  await Actualite.findByIdAndUpdate(req.params.id, req.body, {new: true});
            res.json(actualite);
        }catch (err) {
          console.error(err);
          next(err);
        }
    },

    deleteOne: async (req, res, next) => {
        try {
            let out =  await Actualite.findByIdAndDelete({_id : req.params.id});
            console.log(req.params.id);
            res.json(out);
        }catch (err) {
          console.error(err);
          next(err);
        }
    },
};

module.exports = controller;