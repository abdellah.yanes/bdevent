const { updateOne, deleteOne } = require("../models/proposition.model");
const Proposition = require("../models/proposition.model");

var controller = {

    addLike : async (req, res, next) => {
      try {
        let proposition =  await Proposition.findById({_id : req.params.id});
        if(!proposition.likes.includes(req.user._id)){

          let out = await Proposition.updateOne(
            {_id : req.params.id }, 
            {
              $inc: {nbLikes: 1},
              $push: {likes : req.user._id}
            }
          )
          res.json(out);
        }else{
          res.status(401).send("Unauthorised");
        }
      } catch (err) {
        console.error(err);
        next(err);
      }
    },

    addDislike : async (req, res, next) => {
      try {
        let proposition =  await Proposition.findById({_id : req.params.id});
        if(proposition.likes.includes(req.user._id)){

          let out = await Proposition.updateOne(
            {_id : req.params.id }, 
            {
              $inc: {nbLikes: -1},
              $pull: {likes : req.user._id}
            }
          )
          res.json(out);
        }else{
          res.status(401).send("Unauthorised");
        }
      } catch (err) {
        console.error(err);
        next(err);
      }
    },

    addOne : async (req, res, next) => {
      try {
        const proposition = {"idUser": req.user._id, "nom": req.body.nom,"description": req.body.description};
        let prop = await Proposition.create(proposition);
        res.json(prop);
      }catch (err){
        console.error(err);
        next(err);
      }
    },

    getAll: async (req, res, next) => {
        try {
          let props = await Proposition.find({});
          res.json(props);
        }catch (err){
          console.error(err);
          next(err);
        }
    },

    deleteOne: async (req, res, next) => {
      try {
        let proposition =  await Proposition.findById({_id : req.params.id});
        if(proposition.idUser.equals(req.user._id)){
          let out =  await Proposition.findByIdAndDelete({_id : req.params.id});
          res.json(out);
        }else{
          res.status(401).send("Unauthorised");
        }
      }catch (err) {
        console.error(err);
        next(err);
      }
    },
};

module.exports = controller;