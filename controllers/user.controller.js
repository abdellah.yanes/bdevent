const User = require("../models/user.model");
const jwt = require('jsonwebtoken');
const { hashSync, compareSync } = require('bcrypt');

const config = require('config');
const privateKey = config.get('privateKey');


var controller = {
  getAll: async (req, res, next) => {
    try {
      let users = await User.find({});
      res.json(users);
    }catch (err){
      console.error(err);
      next(err);
    }
  },
  inscription : async (req, res, next) => {
    try {
      const {prenom, nom, mail, mdp} = req.body;

      if(prenom.length<2 || nom.length<2){
        res.status(409).send("Nom ou prénom invalid");

      }else if (!mail.endsWith("@u-paris.fr")){
        res.status(409).send("Mail invalid");

      }else if (mdp.length <8){
        res.status(409).send("Le mot de passe doit contenir au moins 8 caractères");

      }else{
        const users = await User.find({});
        User.findOne({ mail: mail }).then(user => {
          if (!user) {
            let user = User.create({"prenom":prenom, "nom":nom, "mail":mail, "mdp":hashSync(mdp, 10)});
            const payload = {
              mail: mail,
              id: user._id
            }
            const token = jwt.sign(payload, privateKey, { expiresIn: "1d" })
            res.json(token);
          }else{
            res.status(409).send("Mail déjà existant");
          }
        })
      }
    }catch (err){
        console.error(err);
        next(err);
    }
  },
  login : async (req, res, next) => {
    try {
      User.findOne({ mail: req.body.mail }).then(user => {
        if (!user) {
          res.status(401).send("Identifiants incorrects");
        }else if (!compareSync(req.body.mdp, user.mdp)){
          res.status(401).send("Identifiants incorrects");
        }else {
          const payload = {
            mail : user.mail,
            nom : user.nom
          }
  
          const token = jwt.sign(payload, privateKey, { expiresIn: "1d" });
  
          res.send(token);
        }


      })
    }catch (err){
      console.error(err);
      next(err);
    }
  },
    
};

module.exports = controller;