const Event = require("../models/event.model");
const Reservation = require("../models/reservation.model");

var controller = {
    addOne : async (req, res, next) => {
      try {
        let event = await Event.create(req.body);
        res.json(event);
      }catch (err){
        console.error(err);
        next(err);
      }
    },
    getAll: async (req, res, next) => {
      try {
        let events = await Event.find({});
        res.json(events);
      }catch (err){
        console.error(err);
        next(err);
      }
    },
    updateOne : async (req, res, next) => {
      try {
          let events = await Event.findByIdAndUpdate(req.params.id, req.body, {new: false});
          res.json(events);
      }catch (err){
        console.error(err);
        next(err);
      }
    },
    deleteOne: async (req, res, next) => {
      try {
          let out1 = await Event.findByIdAndDelete(req.params.id);
          let out2 = await Reservation.deleteMany({idEvent : req.params.id})
          res.json(out1, out2);
      }catch (err){
        console.error(err);
        next(err);
      }
    },
    addOneReservation: async (req, res, next) => { 
      try{
        let reservations = await Reservation.find({idUser: req.user._id, idEvent: req.params.id});
        if (reservations.length >0){
          res.status(401).send("Réservation déjà existante pour cet événement");
        }else{
          let event = await Event.findById({_id : req.params.id});
          if(event.nbReservations >= event.nbPlaces){
            res.status(401).send("Nombre de places limite atteint");
          }else if(event.onlyAdherant || ! req.user.adherant){
            res.status(401).send("Evenement réservé aux adhérants");
          }else if (event.dateLimiteInscription< Date.now()){
            res.status(401).send("Date limite de réservation dépassée");
          }else{
            let reservation = await Reservation.create({"idUser":req.user._id,"idEvent":event._id});
            let out = await Event.updateOne(
              {_id : req.params.id }, 
              {
                $inc: {nbReservations: 1},
              }
            )
            res.json(reservation);
          }
        }
      }catch(err){
        console.error(err);
        next(err);
      }
    },
    getAllReservations: async (req, res, next) => {
      try{
        if(req.user.admin){
          let reservations = await Reservation.find({});
          res.json(reservations);
        }else{
          let reservations = await Reservation.find({idUser:req.user._id});
          res.json(reservations);
        }
      }catch(err){
        console.error(err);
        next(err);
      }
    },
    updateOneReservation : async (req, res, next) => {
      try {
          let reservations = await Reservation.findByIdAndUpdate(req.params.id, req.body, {new: false});
          res.json(reservations);
      }catch (err){
        console.error(err);
        next(err);
      }
    },
    deleteOneReservation: async (req, res, next) => {
      try{
        if(req.user.admin){
          let out = await Reservation.findByIdAndDelete(req.params.id);
          res.json(out);
        }else{
          let out = await Reservation.deleteOne({_id : req.params.id, idUser : req.user._id});
          res.json(out);
        }

      }catch(err){
        console.error(err);
        next(err);
      }
    }
};


module.exports = controller;