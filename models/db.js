const mongoose = require("mongoose");
const chalk = require("chalk");
const ora = require("ora");
require('dotenv').config();

const options = {};

const url = process.env.DB_URL;
mongoose.connect("mongodb+srv://abdellah_yanes:Abdellah3004@cluster0.zzukd1x.mongodb.net/?retryWrites=true&w=majority", options);

var spinner = ora("Tryping to connect to the database...\n").start();
var connectionOpened = false;

mongoose.connection.on("connecting",function(){
    spinner.start();
});

mongoose.connection.on("error", function(error){
    spinner.stop();
    console.error(chalk.bgKeyword("orange").black("  ERROR "), chalk.keyword("orange")("Error in MongoDB connection "+ error));
}); 

mongoose.connection.on("connected", function(){
    spinner.stop();
    console.error(chalk.bgGreen.black("  CONNECTED  "), chalk.green("Connection to the database successfully established."));  
})

mongoose.connection.once("open", function() {
    connectionOpened = true;
 });
 
 mongoose.connection.on("reconnected", function(){});

 mongoose.connection.on("disconnected", function(){
     if (connectionOpened) {
         console.log(chalk.bgRed.black("  DISCONNECTED  "), chalk.red("Connection to the database lost."));
         spinner = ora("Trying to reconnect to the database...\n").start();
     }
 });

 module.exports = mongoose.connection; 