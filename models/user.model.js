const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema(
    {
        nom : { type: String, required:true },
        prenom : { type: String, required:true },
        mail : { type: String, required:true },
        mdp : { type: String, required:true },
        adherant : { type: Boolean, default: false },
        admin : { type: Boolean, default: false },
    },
    { collection: "users", timestamps: true }
);

module.exports= mongoose.model("User", userSchema);