const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const reservationSchema = new Schema(
    {
        idUser : { type: Schema.Types.ObjectId, ref: 'User', required:true },
        idEvent : { type: Schema.Types.ObjectId, ref: 'Event', required:true },
        valide : { type: Boolean, default: false },
    },
    { collection: "reservations", timestamps: true }
);

module.exports= mongoose.model("Reservation", reservationSchema);