const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const actualiteSchema = new Schema(
    {
        titre : { type: String, required:true },
        description : { type: String, required:true },
        datePublication : { type: Date, required:true },
    }, 
    { collection: "actualites", timestamps: true }
);

module.exports= mongoose.model("Actualite", actualiteSchema);