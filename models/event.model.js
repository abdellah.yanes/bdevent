const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const eventSchema = new Schema(
    {
        nom : { type: String, required:true },
        description : { type: String, required:true },
        nbPlaces : { type: Number, required:true },
        tarif : { type: Number, required:true },
        dateEvent : { type: Date, required:true },
        dateLimiteInscription : { type: Date, required:true },
        dateLimitePaiment : { type: Date, required:true },
        onlyAdherant : { type: Boolean, default: false },
        nbReservations : { type: Number, default: 0 },
    },
    { collection: "events", timestamps: true }
);

module.exports= mongoose.model("Event", eventSchema);