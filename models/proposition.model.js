const mongoose = require("mongoose");
const Schema = mongoose.Schema;
        
const propositionSchema = new Schema(
    {
        nom : { type: String, required:true },
        description : { type: String, required:true },
        idUser : { type: Schema.Types.ObjectId, ref: 'User', required:true },
        nbLikes: { type: Number, default : 0 },
        likes: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    },
    { collection: "propositions", timestamps: true }
);
        
module.exports= mongoose.model("Proposition", propositionSchema);