role = {
    isAdmin : (req, res, next) => {
        if (req.user.admin){
            next();
        }
        else 
            res.status(401).send("Unauthorised");
    },
    isAdherant : (req,res,next) => {
        if (req.user.adherant)
            next(); 
        else 
            res.status(401).send("Unauthorised");
    }
}
module.exports = role;