const Event = require("../models/event.model");
const Reservation = require("../models/reservation.model");

async function job() {
    let events = await Event.find({});
    if(events.length !== 0){
      events.forEach(async event=> {
        if(event.dateLimitePaiment< Date.now()){
          let reservations = await Reservation.find({idEvent : event._id});
          if(reservations.length !== 0){
            reservations.forEach(async reservation=>{
              let out1 = await Reservation.findByIdAndDelete(reservation._id);
              let out2 = await Event.updateOne(
                {_id : event._id }, 
                {
                  $inc: {nbReservations: -1},
                }
              )
              console.log('Job Scheduler : Supression de la reservation',reservation._id);
            })
          }
        }
      });
    }
}

module.exports = job;