require("./models/db");
const cors = require('cors');
const passport = require('passport');
const jobReservation = require('./jobs/reservation.job');
const schedule = require('node-schedule');
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var helmet = require('helmet');

var eventRouter = require('./routes/event.router');
var userRouter = require('./routes/user.router');
var propositionRouter = require('./routes/proposition.router');
var actualiteRouter = require('./routes/actualite.router');


var app = express();

const corsOptions = {
  origin: ['http://localhost:3001', 'https://localhost:3001', 'https://localhost:8080', 'https://localhost:8080'],
  methods: 'GET,POST,PUT,DELETE',
  headers: 'Content-Type,Authorization',
};


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors(corsOptions));
app.use(passport.initialize());
app.use(helmet());

app.use('/event', eventRouter);
app.use('/user', userRouter);
app.use('/proposition', propositionRouter);
app.use('/actualite', actualiteRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//job scheduler 
schedule.scheduleJob('0 0 * * * ', jobReservation);

module.exports = app;