var express = require('express');
var router = express.Router();
var controller = require('../controllers/actualite.controller');

const passport = require('passport');

require('../middlewares/passport');
const role = require('../middlewares/role');
router.post('/', passport.authenticate('jwt', { session: false }), role.isAdmin, controller.addOne);
router.delete('/:id',passport.authenticate('jwt', { session: false }), role.isAdmin, controller.deleteOne);
router.put('/:id',passport.authenticate('jwt', { session: false }), role.isAdmin, controller.updateOne);

router.get('/', controller.getAll);

module.exports = router;