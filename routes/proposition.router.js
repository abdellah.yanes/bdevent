var express = require('express');
var router = express.Router();
var controller = require('../controllers/proposition.controller');

const passport = require('passport');

require('../middlewares/passport');
const role = require('../middlewares/role');
router.post('/', passport.authenticate('jwt', { session: false }), role.isAdherant, controller.addOne);
router.put('/:id/like', passport.authenticate('jwt', { session: false }), controller.addLike);
router.put('/:id/dislike', passport.authenticate('jwt', { session: false }), controller.addDislike);

router.delete('/:id', passport.authenticate('jwt', { session: false }), role.isAdherant, controller.deleteOne);


router.get('/', controller.getAll);

module.exports = router;