var express = require('express');
var router = express.Router();
var controller = require('../controllers/user.controller');

const passport = require('passport');

require('../middlewares/passport');
const role = require('../middlewares/role');
router.get('/',passport.authenticate('jwt', { session: false }), role.isAdmin, controller.getAll);
router.post('/register/', controller.inscription);
router.post('/login/', controller.login);

module.exports = router;