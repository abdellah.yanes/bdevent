var express = require('express');
var router = express.Router();
var controller = require('../controllers/event.controller');

const passport = require('passport');

require('../middlewares/passport');
const role = require('../middlewares/role');

router.get('/', controller.getAll);
router.post('/', passport.authenticate('jwt', { session: false }), role.isAdmin, controller.addOne);
router.put('/:id',passport.authenticate('jwt', { session: false }), role.isAdmin, controller.updateOne );
router.delete('/:id',passport.authenticate('jwt', { session: false }), role.isAdmin, controller.deleteOne); 
router.post('/:id/reservation',passport.authenticate('jwt', { session: false }), controller.addOneReservation);

router.get('/reservation/',passport.authenticate('jwt', { session: false }), controller.getAllReservations);
router.delete('/reservation/:id/',passport.authenticate('jwt', { session: false }), controller.deleteOneReservation);
router.put('/reservation/:id',passport.authenticate('jwt', { session: false }), role.isAdmin, controller.updateOneReservation );


module.exports = router;